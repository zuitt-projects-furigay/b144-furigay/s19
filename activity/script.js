



// get cube
const cube = 2 * 4;
console.log(`The Cube of 2 is ${cube}`)

// Address
const address = [19, "Cade de Amor", "Roxas District", "Quezon City"]
const [housenum , street, barangay, city] = address

console.log(`I live at ${housenum} ${street} ${barangay} ${city}`)



// 

const animal = {
	type: "crocodile",
	habitat: "saltwater",
	weight: "1200 kgs",
	measurement: `20ft 3in` 
}

const {type, habitat, weight, measurement} = animal;

console.log(`lolong was a ${habitat} ${type}. He weighted at ${weight} with a measurement of ${measurement} `) 

// forEach

const number = [1, 2, 3, 4, 5];

number.forEach((number) => console.log(number))



// Add

const add = (x,y,z,a,d) => x + y + z + a + d

let total = add(1,2,3,4,5);
console.log(total);  



// Class

class dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = {}

myDog.name = 'Chantelle';
myDog.age = '11';
myDog.breed = 'Mini Pincher'

console.log(myDog)