/*
	ECMA
		
		European Computer Manufacturers Assosiacition
*/


// --------------------------------------------------------------


/*
	JavaScript ES6 Update

	1. EXPONENT OPERATOR

*/
// Pre-verson
const firstNum = Math.pow(8, 2);
console.log(firstNum);

// ES6
const secondNum = 8 ** 2;
console.log(secondNum);





// -----------------------------------------------------------------


// 2. TEMPLATE LITERAL
// allows us to write string without using theconcat operator (+)
// greatly helps with code readability

// allows us to write strings with embedded  JS expression (${})


//Pre-Template Literal String
// uses single qoute (' ')
let name =  'John'
let message = ' Hello' + name + '! Welcome to programming';
console.log("message without template literals:" + message);


// Strings Using template literals
// uses backticks (``)
message = `Hello ${name}! welcome to programming`;
console.log(`Message with template literals: ${message}`);


// Multi-line using Template Literals
const anotherMessage = `${name} attended a math competition. He won it by solvving the problem 8 ** 2 with solution of ${secondNum}.`
console.log(anotherMessage)


const interestRate = .1;
const principal = 1000;

console.log(`The interest on your saving account is: ${principal * interestRate}`)





// --------------------------------------------------------------------

/*
	3. ARRAY DESTRUCTURING

	allows us to unpack element in arrays into distinc variable.

	allows us to name array element with variable instead of using index numbers. helps with code readability.
	Syntax: les/const [variable, variableNmae] = array


*/
const fullName = ["Juan", "Dela", "Cruz"]

// Pre array Destructuring
console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! Its nice to meet you`)


// /ArrayDestruturing
const [firstName , middleName, LastName] = fullName

console.log(firstName)
console.log(middleName)
console.log(LastName)

// Expression are any valid uunit of code that resolves a value.
console.log(`Hello ${firstName} ${middleName} ${LastName}!  It's nice to meet you`)






// --------------------------------------------------
/*
	4. OBJECT DESTRUCTURING

		allows us to unpack element in object into distinct variable.
*/

const person = {
	givenName: 'JJane',
	maidenName: 'Dela',
	familyName: 'Cruz'
}


// Pre-Object Destructuring
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's nice to see you again`)



// Object Destructuring
// shortens the syntax for accessing pproperties from objects.
// sytax: let /const{propertyName, propertyName} = object

const{ givenName, maidenName, familyName} = person;

console.log(givenName)
console.log(maidenName)
console.log(familyName)

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's nice to see you again`)


// Use to function
function getFullName({givenName, maidenName, familyName}){
	console.log(`${givenName}, ${maidenName},${familyName}`)
}

getFullName(person)






// ----------------------------------------------------------------------

/*
	5. ARROW FUNCTION

		Compact alternative syntax to traditional function

		useful for code snippets where creating functioin will not be reused in any ather portion of the code

		adheres to the 'DRY' principle (Dont repeat yourself) where theres no longer need to create a function and think of a name for function that only be used in certain snippets.
*/

// Normal function
function printFullName(fName, mName, lName){
	console.log(fName + ' ' + mName + ' ' + lName)

}
// function declaration
// function name
// parameters = placeholder/ the name of an argument to be passed to the function
// statement =function bidy
// invoke/ call backfunc

printFullName('John', 'D.', 'Smith')


// ES6 arrow function
// 
const variableName = () => {

}

const printFName = (fName, mName, lName) => {console.log(`${fName} ${mName} ${lName}`)
}
printFName('Jane', 'D.', 'Smith')


// Arrow Funtion with loops 
// Pre- Arrow function

const students = ['John', 'Jane', 'Joe']

students.forEach(function(student){
	console.log(`${student} is a student`)
})


// arrow function
students.forEach((student) => console.log(`${student} is a student.`));




// --------------------------------------------------

/*
	6. (Arrow Function) IMPLICIT RETURN STATEMENT
		
		There are instances when you can omit the 'return' statement. This wo


*/
// Pre-arrow function
// const add = (x,y) => {return x + y;} 

// let total = add(1,2);
// console.log(total);

// Arrow Function
const add = (x,y) => x + y

let total = add(1,2);
console.log(total);


// let filterFriend = friends.filter(friend => friend.length === 4);






// --------------------------------------------------

/*
	7. (Arrow Function) DEFAULT FUNCTION ARGuMENT VALUE

		provides a default argument value if none is proovided when the function is invoked
*/

const greet = (name = 'User') => {return `Good moorning ${name}`}

console.log(greet());



// --------------------------------------------------

/*
	8. CREATING A CLASS

		Syntax:

			class className {
				constructor(objectPropertyA, objectPropertyB){
					this.objectPropertyA =
					objectPropertyA;
					this.ObjectPropertyB = 
					objectPropeertyB
				}
			}

		The constructor is a special method of a class for creating/initializing an object for that class.

		The 'this'
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar)

// Value of properties may be assigned after creation/ instantiation of an object
myCar.brand = 'Ford';
myCar.name = 'Ranger Raptor';
myCar.year = 2021;

console.log(myCar)

// 
const myNewCar = new Car('Toyota', 'Vios', 2021)

console.log(myNewCar)




// -----------------------------------------------------
/*
	9. TERNARY OPERATOR

		Conditional operator

		It has tree operands

		Condition, followed by Question mark ?, then and expression to execute if the condition id truth followed by colon (:) and finally the expression to execute if the condition is false.

*/

// if(condition == 0){
// 	return true
// }else{
// 	false
// }



// // Ternary Operator
// (condition == 0) ? true : false
